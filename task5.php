<?php

$array = [
    0 => 7,
    1 => 1,
    2 => 4,
    3 => 2,
    4 => 5
];

$arrayCount = count($array);

/**
 * Sort Array
 * @param $array
 * @param $count
 * @return mixed
 */
function sortArray($array, $count){
    for($i=0; $i < $count-1; $i++)
    {
        $small = $i;

        for($k=$i+1; $k < $count; $k++)
        {
            if($array[$k] < $array[$small])
            {
                $small = $k;
            }
        }

        $buffer = $array[$i];
        $array[$i] = $array[$small];
        $array[$small] = $buffer;
    }

    return $array;

}

print_r(sortArray($array, $arrayCount));