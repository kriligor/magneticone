<?php
$firstArray = [
    'el', 'ab', 'cd'
];

$secondArray = [
    'y5', 'y6', 'y7'
];

/**
 * Combine Array
 * @param $firstArray
 * @param $secondArray
 * @return array
 */
function combine($firstArray, $secondArray) {
    arsort($secondArray);
    $array = array_combine($firstArray, $secondArray);

    return $array;
}

print_r(combine($firstArray,$secondArray));