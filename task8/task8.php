<?php

$arrayFirst = [
    'name' => 'some name',
    'age' => 5,
    'city' => 'some town'
];

$arraySecond = [
    'age' => 6,
    'country' => 'small country',
    'city' => 'mego city',
    'street' => 'cute ave.'
];

$arrayThird= ['name' =>'',
    'age'=>'',
    'country' =>'',
    'city' =>'',
    'street' =>''
];

$file = fopen('task8.csv', 'w');
fputcsv($file, array_keys($arrayThird));
fputcsv($file, array_replace($arrayThird, $arrayFirst));
fputcsv($file, array_replace($arrayThird, $arraySecond));
fclose($file);
