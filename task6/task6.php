<?php

/**
 * Scan File
 * @param $file
 * @return string
 */
function scanFile($file)
{
    $res ='';

    foreach ($file as $key) {
        if(preg_replace("/[^0-9]/", '', $key) > 9){
            $res .= $key . '<br>';
        }
    }

    echo $res;

    return $res;
}

/**
 * Scan Directory
 * @return string
 */
function scanDirectory()
{
    $file = scandir('.');

    return scanFile($file);
}

scanDirectory();