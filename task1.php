<?php

$array = [
    'test.mail@mail.com',
    'abc-mail@host.ua',
    'user@site.net',
    'nomail@gmail.coma',
    '-mnk@mail.com',
    'mail@mail@mail.com',
    'mail*tt@mail.com'
];

$regular = '/^([a-zA-Z0-9]+[a-zA-Z0-9\._\-]+@[a-zA-Z0-9\._\-]+(\.[a-zA-Z0-9]{2,3}+)+)*$/';

foreach ($array as $mail) {
    if (preg_match($regular, $mail)) {
        echo $mail . ' Email is correct' . '</br>';
    } else {
        echo $mail . ' Email is not correct' . '</br>';
    }
}