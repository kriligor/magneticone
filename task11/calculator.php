<?php

/**
 * Calculator
 * @param $valueFirst
 * @param $valueSecond
 * @param $operation
 * @return array
 */
function calculator($valueFirst, $valueSecond, $operation)
{
    $result = [];
    if ($valueFirst === '' && $valueSecond === '') {
        echo 'Enter a value';
    } else {
        if ($operation == '+') {
           echo $valueFirst + $valueSecond;
        } elseif ($operation == '-') {
            echo $valueFirst - $valueSecond;
        } elseif ($operation == '*') {
            echo $valueFirst * $valueSecond;
        } elseif ($operation == '/') {
            if ($valueSecond == 0) {
                echo 'Division by zero is impossible';
            } else {
                echo $valueFirst / $valueSecond;
            }
        }
    }

    return $result;
    
}