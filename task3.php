<?php

$firstArray = [
    7 , 1, 4, 2, 5
];

$secondArray = [
    'a11', 'a3', 'a22', 'a11'
];

sort($firstArray);
unset($secondArray[0]);
array_unshift($secondArray, 'a1');

/**
 * Get Number
 * @param $number
 * @return mixed
 */
function getNumber($number)
{
    return preg_replace("/[^0-9]/", '', $number);
}

for($i=0; $i <count($secondArray); $i++){
    $right = $secondArray[$i];
    for ($j = $i - 1; $j >= 0 && getNumber($secondArray[$j]) > getNumber($right); $j--) {
        $secondArray[$j + 1] = $secondArray[$j];
    }
    $secondArray[$j + 1] = $right;
}

print_r($secondArray);

print_r($firstArray);