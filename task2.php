<?php

/**
 * Translate
 * @param $str
 * @return mixed
 */
function translate($string)
{

    $ukraine = [
        'а', 'б', 'в', 'г', 'д', 'е', 'є', 'ж',
        'з', 'и','і', 'й', 'к', 'л', 'м', 'н',
        'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х',
        'ц', 'ч', 'ш', 'щ', 'ь', 'ю', 'я'
    ];

    $english = [
        'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh',
        'z','u', 'i', 'y', 'k', 'l', 'm', 'n',
        'o', 'p', 'r', 's', 't','u', 'f', 'h',
        'c', 'sh', 'sh', 'sch', 'y', 'ya'
    ];

    return str_replace($ukraine, $english, $string);
}

echo translate("певна стрічка");